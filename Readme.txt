This is a JEE project built purely using Servlet and JSP. This is a student management system
where typical CRUD operations can be performed. 

The project uses Java EE 7 and JDK 1.7.  The project is built around MVC architecture. 
The View pages are written using HTML5, CSS3 and JSTL.
Controller class is written as Servlet and the Models are plain java classes. MySQL is used as the 
database server. JDBC is used to connect to the database and Data Access Object (DAO) pattern was used to 
operate on the database.  

In order to run this project in Eclipse, import it as a dynamic web project and use Tomcat as the web server and run it.
Alternatively, you can deploy the build\web-student-tracker.war to your web server. 
    