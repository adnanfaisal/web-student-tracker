package com.adnan.web.jdbc;

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;


import javax.annotation.Resource;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.sql.DataSource;

/**
 * Servlet implementation class TestJdbc
 */
@WebServlet("/TestJdbc")
public class TestJdbc extends HttpServlet {
	private static final long serialVersionUID = 1L;
	
	//1. Define connection pool /datasource for resource injection
	@Resource(name="jdbc/web_student_tracker") /* must match with context.xml */
	private DataSource dataSource;

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		PrintWriter out = response.getWriter();
		response.setContentType("text/plain");
		
		Connection connection = null;
		Statement statement = null;
		ResultSet resultSet = null;
		
		try{
			// 2. get connection 
			connection = dataSource.getConnection();
			
			// 3. create statement
			String sql = "SELECT * from student";			
			statement = connection.createStatement();
			
			// 4. execute query
			resultSet = statement.executeQuery(sql);
			
			// 5. process result set 
			while(resultSet.next()){
				String email = resultSet.getString("email");
				out.println(email);
			}
			
		}
		catch (Exception ex){
			ex.printStackTrace();
		}
	}

}
