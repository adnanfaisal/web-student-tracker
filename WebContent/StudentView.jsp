<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ page import="java.util.*, com.adnan.web.jdbc.*"  %>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Web Student Tracker</title>

<link rel="stylesheet" type="text/css" href="./css/style.css">
</head>
<%
	List<Student> students = (List<Student>) request.getAttribute("studentList");
%>
<body>
<p>
<div id="wrapper">
	<div id="header">
		<h2> Student Information </h2>
	</div>
</div>
<table>
<div id="container">
	<div id="content">
		<thead>
			<th> First Name </th>
			<th> Last Name </th>
			<th> E-mail </th>
		</thead>
		
		<tbody>
		
			<% for(Student student:students) { %>
				<tr>
			 	<td> <%=  student.getFirstName() %>
			 	<td> <%=  student.getLastName() %>
			 	<td> <%=  student.getEmail() %>
			 	 </tr>
			 <% } %>	  
		
		</tbody>
		</table>
		</div>
	
</div>
</body>
</html>