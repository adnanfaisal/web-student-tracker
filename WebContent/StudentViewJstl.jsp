<!--  This is an alternative implemenation of StudentView.jsp using JSTL tag. -->
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %> 

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<link rel="stylesheet" type="text/css" href="css/style.css">
<title>Student Tracker App</title>
</head>
<body>

<p>
<h2> Student Information</h2>
<p>
<input type="submit" value="Add new student" 
onclick="window.location.href='AddStudentForm.jsp'; return false;"
class="add-student-button">
<p>
<table>
	<thead>
		<th> First Name </th>
		<th> Last Name </th>
		<th> E-mail </th>
		<th> Update </th>
	</thead>

<tbody>
	<c:forEach var="student" items="${studentList}">
	<c:url var="updateLink" value="StudentControllerServlet"> <!--  Used to create URL with parameter -->
		<c:param name="command" value="Retrieve" />
		<c:param name="studentId" value="${student.id}" />		
	</c:url>
	<c:url var="deleteLink" value="StudentControllerServlet"> <!--  Used to create URL with parameter -->
		<c:param name="command" value="Delete" />
		<c:param name="studentId" value="${student.id}" />		
	</c:url>
	 <tr>
	 	<td> ${student.firstName}
	 	<td> ${student.lastName}
	 	<td> ${student.email}
	 	<td> <a href="${updateLink}" > 
	 	      Update  </a> | 
	 	      <a href="${deleteLink}"
	 	      onclick="if (!(confirm('Are you sure you want to delete this student?'))) return false">
	 	      Delete  </a>    
	 </tr>
	</c:forEach>
</tbody>
</table>

</body>
</html>