package com.adnan.web.jdbc;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.sql.DataSource;

/**
 * This is a DAO class that queries on the DataBase and returns result. 
 * @author adnan
 *
 */
public class StudentDbUtil {

	DataSource dataSource = null;
	
	public StudentDbUtil(DataSource dataSource){
		this.dataSource = dataSource;
	}
	
	public List<Student> getStudents() throws SQLException{
		List<Student> students = new ArrayList<>();
		
		Connection connection = null;
		ResultSet resultSet = null;
		Statement statement = null;
		
		
		try {
			connection = dataSource.getConnection();
			
			String query = "SELECT * from student order by last_name";
			statement = connection.createStatement();
			resultSet = statement.executeQuery(query);
			
			
			while(resultSet.next()){
				int id = resultSet.getInt("id"); // database column name
				String firstName = resultSet.getString("first_name"); // database column name
				String lastName = resultSet.getString("last_name");
				String email = resultSet.getString("email");
				Student student = new Student(id,firstName,lastName,email);
				students.add(student);
			}
		} 		
		finally{
			close(connection, statement, resultSet);
		}		
		return students;
		
	}

	public  void addStudent(Student student) throws SQLException{
		Connection connection = null;
		PreparedStatement statement = null; // since we need to prepare the student based on passed parameters
		
		
		try {
			connection = dataSource.getConnection();
			
			String query = "insert into student (first_name, last_name, email) values (?,?,?)";
			statement = connection.prepareStatement(query);
			
			statement.setString(1, student.getFirstName()); // param value index starts from 1 instead of 0
			statement.setString(2, student.getLastName());
			statement.setString(3, student.getEmail());
			
			//execute sql insert
			statement.execute();
		} 
				
		finally{
			close(connection, statement, null);
		}		
		
		
	}

	public Student getStudent(String studentId) throws SQLException {

		Student student = null;
		
		Connection connection = null;
		PreparedStatement statement = null; // since we need to prepare the student based on passed parameters		
		ResultSet resultSet = null;

		int studentIdInt = Integer.parseInt(studentId);
		
		
		try {
				
			connection = dataSource.getConnection();
			
			String query = "SELECT * from student where id=?";
			statement = connection.prepareStatement(query);
			statement.setInt(1,studentIdInt);				
			resultSet = statement.executeQuery();
			
			if(resultSet.next()){
				int id = resultSet.getInt("id"); // database column name
				String firstName = resultSet.getString("first_name"); // database column name
				String lastName = resultSet.getString("last_name");
				String email = resultSet.getString("email");
				student = new Student(id,firstName,lastName,email);
				return student;
			}
			else{
				throw new SQLException("Could not find student id: " + studentId);
			}
				
		} 
			
		finally{
			close(connection, statement, resultSet);
		}		
		
	}


	public void updateStudent(Student student)throws SQLException {
			Connection connection = null;
			PreparedStatement statement = null; // since we need to prepare the student based on passed parameters
			
			
			try {
				connection = dataSource.getConnection();
				
				String query = "update student " +
							   "set first_name=?, last_name=?, email=? " +
							   "where id=?";
				statement = connection.prepareStatement(query);
				
				statement.setString(1, student.getFirstName()); // param value index starts from 1 instead of 0
				statement.setString(2, student.getLastName());
				statement.setString(3, student.getEmail());
				statement.setInt(4, student.getId());
							
				//execute sql insert
				statement.execute();
			} 
					
			finally{
				close(connection, statement, null);
			}		
			
		
	}

	public void deleteStudent(int studentId) throws SQLException {
		
		Connection connection = null;
		PreparedStatement statement = null; // since we need to prepare the student based on passed parameters		
		ResultSet resultSet = null;
		
		try {
				
			connection = dataSource.getConnection();
			
			String query = "delete from student where id=?";
			statement = connection.prepareStatement(query);
			statement.setInt(1,studentId);				
			int numDeleted = statement.executeUpdate();
			
			if(numDeleted == 0){			
				throw new SQLException("Could not find student with id: " + studentId);
			}
				
		} 
			
		finally{
			close(connection, statement, resultSet);
		}	
		
	}
	
	private void close(Connection connection, Statement statement,
			ResultSet resultSet) {
		try {
			if(resultSet != null) resultSet.close();
			if(statement != null) statement.close();
			if(connection != null) connection.close(); // doesn't really close it, just puts it back to the connection pool
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}
	
}
