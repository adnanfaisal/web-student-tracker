<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<link rel="stylesheet" type="text/css" href="css/style.css">

<title>Update student</title>
</head>
<body>
<div id="wrapper">
	<div id="header">
		<h1>Add a new student</h1>
		<h2> New Student Information </h2>
	</div>
</div>

<div id="container">
		<form action="StudentControllerServlet" method="get">
		
		<input type="hidden" name="command" value="Update"> <!-- hidden command button to pass values to servlet --> 
		<input type="hidden" name="studentId" value="${theStudent.id}"> <!-- hidden command button to pass values to servlet --> 
		
		<!--  note that all the "input" element types are passed to the servlet through request parameter by default.
		So, the values provided by the user in the following input boxes are also passed to the servlet. -->
		
		<div id="content">
			<p> <label>First Name: </label> <input type="text" maxlength="30" name="firstName"
									value="${theStudent.firstName}"> <!-- theStudent was set as requestAttribute in the servlet's loadStudent method -->
			<p> <label>Last Name: </label> <input type="text" maxlength="30" name="lastName"
									value="${theStudent.lastName}"> 
			<p> <label>Email: </label> <input type="text" maxlength="30" name="email"
									value="${theStudent.email}"> 
			<p><input type="submit" value="Save student" class="save">
		</div>
		</form>
</div>
<div style="clear: both;"></div>
<p> <a href="StudentControllerServlet"> Back to student list </a>
</body>
</html>