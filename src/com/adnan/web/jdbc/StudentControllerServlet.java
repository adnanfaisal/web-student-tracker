package com.adnan.web.jdbc;

import java.io.IOException;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import javax.annotation.Resource;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.sql.DataSource;

/**
 * Servlet implementation class StudentControllerServlet
 */
@WebServlet("/StudentControllerServlet")
public class StudentControllerServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	@Resource(name="jdbc/web_student_tracker") 
	DataSource dataSource;
	List<Student> students = new ArrayList<>();
	StudentDbUtil studentDbUtil;  
	
	
	@Override
	public void init() throws ServletException {
	
		super.init();
		try{
			studentDbUtil = new StudentDbUtil(dataSource);
		}
		catch(Exception ex){
			throw new ServletException(ex);
		}
	}



	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)  {
		
		String command = request.getParameter("command");
		if(command == null)
			command = "LIST";
		
		try {
			switch (command){
				case "List": 
					listStudents(request, response);
					break;
				case "Add":
					addStudent(request, response);
					break;
				case "Retrieve":
					loadStudent(request, response);
					break;
				case "Update":
					updateStudent(request, response);
					break;
				case "Delete":
					deleteStudent(request, response);
					break;
				default:			
					listStudents(request, response);				 
			}
		} catch (ServletException | SQLException | IOException e) {
			System.err.println("Exception occured: " + e);
			e.printStackTrace();

		}
	
	}



	private void deleteStudent(HttpServletRequest request,
			HttpServletResponse response) throws SQLException,  IOException, ServletException {
		Integer studentId = Integer.parseInt(request.getParameter("studentId"));
		System.out.println("Delete  student with id: " + studentId);
		new StudentDbUtil(dataSource).deleteStudent(studentId);
		listStudents(request, response);
		
	}



	private void updateStudent(HttpServletRequest request,
			HttpServletResponse response) throws SQLException, IOException, ServletException {
		
		Integer studentId = Integer.parseInt(request.getParameter("studentId"));
		String firstName = request.getParameter("firstName");
		String lastName = request.getParameter("lastName");
		String email = request.getParameter("email");
		Student student = new Student(studentId, firstName, lastName, email);
		new StudentDbUtil(dataSource).updateStudent(student);	
		listStudents(request, response);
	
	}



	private void loadStudent(HttpServletRequest request,
	    HttpServletResponse response) throws SQLException, IOException, ServletException {
		String studentId = request.getParameter("studentId");
		Student student = new StudentDbUtil(dataSource).getStudent(studentId);
		request.setAttribute("theStudent", student);
		RequestDispatcher dispatcher = request.getRequestDispatcher("/UpdateStudentForm.jsp");
		dispatcher.forward(request, response);
		
	}



	private void addStudent(HttpServletRequest request,
			HttpServletResponse response) throws SQLException, ServletException, IOException {
		
		String firstName = request.getParameter("firstName");
		String lastName = request.getParameter("lastName");
		String email = request.getParameter("email");
		Student student = new Student(firstName,lastName,email);
		
		
		studentDbUtil.addStudent(student);
		listStudents(request, response);
		
	}



	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp)
			throws ServletException, IOException {
		System.out.println("Inside post");	
	}



	private void listStudents(HttpServletRequest request,
			HttpServletResponse response) throws SQLException, IOException, ServletException {
		
		students = studentDbUtil.getStudents();
		request.setAttribute("studentList",students);
		
		// Keep only one of the following lines. 
		RequestDispatcher dispatcher = request.getRequestDispatcher("/StudentViewJstl.jsp"); // use jstl
	//	RequestDispatcher dispatcher = request.getRequestDispatcher("/StudentView.jsp"); // use scriplet
		
		dispatcher.forward(request, response);		
	}

}
